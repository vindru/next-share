import { GetServerSideProps } from 'next'
import React from 'react'
import config from '../../utils/config'

const Index: React.FC = () => null
export const getServerSideProps: GetServerSideProps = async ({ res, query }) => {
    const { appUrl } = config(query)
    res.setHeader('location', appUrl)
    res.statusCode = 302
    res.end()
    return { props: {} }
}
export default Index
