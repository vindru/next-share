import { GetServerSideProps } from 'next'
import React from 'react'
import Head from 'next/head'
import config from '../../utils/config'
import getNewDate from '../../utils/getNewDate'

interface Props {
    concert: any
    appUrl: string
}

const DEVICE = 'web'

const Concert: React.FC<Props> = ({ concert, appUrl }) => {
    const title = concert?.name ?? ''
    const description = concert?.description ?? ''
    const image = concert?.cover?.url ?? ''
    const url = `${appUrl}/concert/F3DAS${concert.id}2RDFS/${concert.name}`
    React.useEffect(() => {
        if (typeof window !== 'undefined') {
            window.location.href = url
        }
    }, [])
    return (
        <Head>
            <meta charSet="utf-8" />
            <meta name="twitter:card" content="summary_large_image" />
            <meta property="og:type" content="website" />
            {title && (
                <>
                    <title>{title}</title>
                    <meta name="title" content={title} />
                    <meta name="twitter:title" content={title} />
                    <meta name="og:title" content={title} />
                </>
            )}
            {description && (
                <>
                    <meta name="description" content={description} />
                    <meta name="twitter:description" content={description} />
                    <meta name="og:description" content={description} />
                </>
            )}
            {image && (
                <>
                    <meta name="image" content={image} />
                    <meta name="twitter:image" content={image} />
                    <meta name="og:image" content={image} />
                </>
            )}
            {url && <meta name="og:url" content={url} />}
        </Head>
    )
}
export const getServerSideProps: GetServerSideProps = async ({ params, query }) => {
    const { appUrl, apiUrl } = config(query)
    var result = params.id.slice(5,-5);
    const singleConcertApi = `${apiUrl}/concerts/schedule/${result}`
    const data = await fetch(singleConcertApi, {
        headers: {
            Accept: '*/*',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'x-app-platform': DEVICE,
            'cache-control': 'no-cache',
            'x-app-device': DEVICE,
            'x-app-date': getNewDate(),
        },
    }).then((res) => res.json())
    const concert = data ? data.concert_schedule : null
    return {
        props: {
            concert,
            appUrl,
        },
    }
}

export default Concert
