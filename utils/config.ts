import { ParsedUrlQuery } from 'querystring'

const config = (query: ParsedUrlQuery) => ({
    apiUrl: query.apiUrl || process.env.NEXT_APP_API_URL,
    appUrl: query.appUrl || process.env.NEXT_APP_APP_URL,
})

export default config
